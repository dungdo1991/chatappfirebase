//
//  ViewModelType.swift
//  cleanarchitecture
//
//  Created by Dung Do on 18/01/2022.
//

import Foundation

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    func tranform(input: Input) -> Output
}
