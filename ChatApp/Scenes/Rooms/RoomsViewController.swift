//
//  RoomsViewController.swift
//  ChatApp
//
//  Created by Dung Do on 24/01/2022.
//

import UIKit
import RxSwift
import RxCocoa

class RoomsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addRoomButton: UIButton!
    
    var roomsVM: RoomsViewModel?
    private lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        return searchBar
    }()
    
    deinit {
        print("deinit - RoomsViewController")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupRx()
        tableView.setContentOffset(CGPoint(x: 0, y :CGFloat.greatestFiniteMagnitude), animated: false)
    }
    
    private func setupView() {
        title = "Rooms"
        
        tableView.registerClass(UITableViewCell.self)
        tableView.delegate = self
        
        navigationItem.titleView = searchBar
    }
    
    private func setupRx() {
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = RoomsViewModel.Input(viewWillAppear: viewWillAppear,
                                         nextPageTrigger: tableView.rx.reachedBottom,
                                         tapRoom: tableView.rx.itemSelected.asDriver(),
                                         tapCreateRoom: addRoomButton.rx.tap.asDriver(),
                                         searchRoom: searchBar.rx.text.orEmpty.asDriver())
        
        let output = roomsVM?.tranform(input: input)
        output?.error
            .drive(errorBinding)
            .disposed(by: disposeBag)
        output?.loading
            .drive(loadingBinding)
            .disposed(by: disposeBag)
        output?.rooms
            .drive(tableView.rx.items(cellIdentifier: UITableViewCell.reuseID, cellType: UITableViewCell.self)) { index, room, cell in
                cell.selectionStyle = .none
                cell.textLabel?.text = room.id
            }
            .disposed(by: disposeBag)
        output?.actionComplete
            .drive()
            .disposed(by: disposeBag)
    }
    
    func sum(a: Int, b: Int) -> Int {
        return a + b
    }

}

extension RoomsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}

extension RoomsViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
}
