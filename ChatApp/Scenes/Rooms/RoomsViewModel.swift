//
//  RoomsViewModel.swift
//  ChatApp
//
//  Created by Dung Do on 24/01/2022.
//

import Foundation
import RxSwift
import RxCocoa

class RoomsViewModel: ViewModelType {
    
    private let navigator: RoomsNavigator
    private let userUseCase: UserUseCase
    private let chatUseCase: ChatUseCase
    
    init(userUseCase: UserUseCase, chatUseCase: ChatUseCase, navigator: RoomsNavigator) {
        self.userUseCase = userUseCase
        self.chatUseCase = chatUseCase
        self.navigator = navigator
    }
    
    func tranform(input: Input) -> Output {
        let errorTracker = ErrorTracker()
        let activityIndicator = ActivityIndicator()
        var rooms = [Room]()
        
        let datas = Driver.combineLatest(input.searchRoom, input.nextPageTrigger)
            .flatMapLatest { text, _ -> Driver<[Room]> in
                if text.isEmpty {
                    return self.chatUseCase.observerRooms(skip: rooms.count, limit: 15)
                        .trackError(errorTracker)
                        .asDriverOnErrorJustComplete()
                        .do { newRooms in
                            rooms = newRooms
                        }
                } else {
                    return self.chatUseCase.removeObserverRooms()
                        .andThen(Observable.just(text))
                        .asDriverOnErrorJustComplete()
                        .flatMapLatest { text in
                            self.chatUseCase.getRoomFrom(roomID: text)
                                .trackError(errorTracker)
                                .asDriverOnErrorJustComplete()
                                .map { room -> [Room] in
                                    if let room = room {
                                        return [room]
                                    } else {
                                        return []
                                    }
                                }
                                .do { room in
                                    rooms.removeAll()
                                    rooms.append(contentsOf: room)
                                }
                        }
                }
            }
        
        let tapRoom = input.tapRoom
            .do { indexPath in
                self.navigator.goToChat(roomID: rooms[indexPath.row].id)
            }
            .mapToVoid()
        
        let tapCreateRoom = input.tapCreateRoom
            .flatMapLatest { _ in
                self.chatUseCase.createRoom()
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
            }
            .do { room in
                self.navigator.goToChat(roomID: room.id)
            }
            .mapToVoid()
        
        let viewWillAppear = input.viewWillAppear
            .flatMapLatest { _ -> Driver<Void> in
                if Session.shared.currentUser == nil {
                    return self.userUseCase.createUser()
                        .do { user in
                            Session.shared.currentUser = user
                        }
                        .asDriverOnErrorJustComplete()
                        .mapToVoid()
                } else {
                    return Driver.just(())
                }
            }
        
        let actionComplete = Driver.merge(tapRoom, tapCreateRoom, viewWillAppear)
        
        return Output(error: errorTracker.asDriver(),
                      loading: activityIndicator.asDriver(),
                      rooms: datas,
                      actionComplete: actionComplete)
    }
    
}

extension RoomsViewModel {
    struct Input {
        let viewWillAppear: Driver<Void>
        let nextPageTrigger: Driver<Void>
        let tapRoom: Driver<IndexPath>
        let tapCreateRoom: Driver<Void>
        let searchRoom: Driver<String>
    }
    struct Output {
        let error: Driver<Error>
        let loading: Driver<Bool>
        let rooms: Driver<[Room]>
        let actionComplete: Driver<Void>
    }
}
