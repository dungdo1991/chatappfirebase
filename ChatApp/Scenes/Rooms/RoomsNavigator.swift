//
//  RoomsNavigator.swift
//  ChatApp
//
//  Created by Dung Do on 24/01/2022.
//

import UIKit

protocol RoomsNavigator: AnyObject {
    func goToChat(roomID: String)
}

class DefaultRoomsNavigator: RoomsNavigator {
    
    private let navigation: UINavigationController
    private let useCaseProvider: UseCaseProvider
    
    init(navigation: UINavigationController, useCaseProvider: UseCaseProvider) {
        self.navigation = navigation
        self.useCaseProvider = useCaseProvider
    }
    
    func goToChat(roomID: String) {
        let nav = DefaultChatNavigator(navigation: navigation, useCaseProvider: useCaseProvider)
        let viewModel = ChatViewModel(navigator: nav,
                                      userUseCase: useCaseProvider.createUserUseCase(),
                                      chatUseCase: useCaseProvider.createChatUseCase(),
                                      roomID: roomID)
        let chatVC = ChatViewController()
        chatVC.chatVM = viewModel
        navigation.pushViewController(chatVC, animated: true)
    }
    
}
