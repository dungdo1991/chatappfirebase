//
//  ChatViewController.swift
//  ChatApp
//
//  Created by Dung Do on 26/01/2022.
//

import UIKit
import RxSwift
import RxCocoa

class ChatViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var sendButton: UIButton!
    var chatVM: ChatViewModel?
    
    deinit {
        print("deinit - ChatViewController")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupRx()
        tableView.setContentOffset(CGPoint(x: 0, y :CGFloat.greatestFiniteMagnitude), animated: false)
    }
    
    private func setupView() {
        title = "Chat"
        
        tableView.register(MyChatTableViewCell.self)
        tableView.register(OtherChatTableViewCell.self)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
    }
    
    private func setupRx() {
        let viewWillDisappear = rx.sentMessage(#selector(UIViewController.viewWillDisappear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = ChatViewModel.Input(viewWillDisappear: viewWillDisappear,
                                        nextPageTrigger: tableView.rx.reachedBottom,
                                        message: messageTextView.rx.text.orEmpty.asDriver(),
                                        tapSend: sendButton.rx.tap.asDriver())
        
        let output = chatVM?.tranform(input: input)
        output?.error
            .drive(errorBinding)
            .disposed(by: disposeBag)
        output?.loading
            .drive(loadingBinding)
            .disposed(by: disposeBag)
        output?.messages
            .drive(tableView.rx.items) { tableView, row, message in
                if message.senderID == Session.shared.currentUser?.id {
                    let cell = tableView.dequeueReusableCell(ofType: MyChatTableViewCell.self)
                    cell.message = message
                    cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(ofType: OtherChatTableViewCell.self)
                    cell.message = message
                    cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
                    return cell
                }
            }
            .disposed(by: disposeBag)
        output?.sentMessage
            .drive(onNext: { _ in
                self.messageTextView.text = ""
            })
            .disposed(by: disposeBag)
        output?.actionComplete
            .drive()
            .disposed(by: disposeBag)
    }

}
