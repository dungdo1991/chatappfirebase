//
//  ChatViewModel.swift
//  ChatApp
//
//  Created by Dung Do on 26/01/2022.
//

import Foundation
import RxSwift
import RxCocoa

class ChatViewModel {
    
    private let navigator: ChatNavigator
    private let userUseCase: UserUseCase
    private let chatUseCase: ChatUseCase
    private let roomID: String
    
    init(navigator: ChatNavigator, userUseCase: UserUseCase, chatUseCase: ChatUseCase, roomID: String) {
        self.navigator = navigator
        self.userUseCase = userUseCase
        self.chatUseCase = chatUseCase
        self.roomID = roomID
    }
    
    func tranform(input: Input) -> Output {
        let errorTracker = ErrorTracker()
        let activityIndicator = ActivityIndicator()
        var messages = [Message]()
        
        let nextPage = input.nextPageTrigger
            .flatMapLatest { _ -> Driver<[Message]> in
                self.chatUseCase.observerMessages(from: self.roomID, skip: messages.count, limit: 15)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
            }
            .map { newMessages -> [Message] in
                messages = newMessages
                return messages
            }
        
        let sentMessage = input.tapSend
            .withLatestFrom(input.message)
            .flatMapLatest { message -> Driver<Void> in
                if message.isEmpty {
                    return Driver.just(())
                } else {
                    return self.chatUseCase.chat(content: message, roomID: self.roomID, senderID: Session.shared.currentUser?.id ?? "Unknown")
                        .trackError(errorTracker)
                        .asDriverOnErrorJustComplete()
                        .mapToVoid()
                }
            }
        
        let viewWillDisappear = input.viewWillDisappear
            .flatMapLatest { _ in
                self.chatUseCase.removeObserverMessages()
                    .andThen(Observable.just(()))
                    .asDriverOnErrorJustComplete()
            }
        
        return Output(error: errorTracker.asDriver(),
                      loading: activityIndicator.asDriver(),
                      messages: nextPage,
                      sentMessage: sentMessage,
                      actionComplete: Driver.merge(viewWillDisappear))
    }
}

extension ChatViewModel {
    struct Input {
        let viewWillDisappear: Driver<Void>
        let nextPageTrigger: Driver<Void>
        let message: Driver<String>
        let tapSend: Driver<Void>
    }
    struct Output {
        let error: Driver<Error>
        let loading: Driver<Bool>
        let messages: Driver<[Message]>
        let sentMessage: Driver<Void>
        let actionComplete: Driver<Void>
    }
}
