//
//  ChatNavigator.swift
//  ChatApp
//
//  Created by Dung Do on 26/01/2022.
//

import UIKit

protocol ChatNavigator: AnyObject {
    func goRooms()
}

class DefaultChatNavigator: ChatNavigator {
    
    private let navigation: UINavigationController
    private var useCaseProvider: UseCaseProvider
    
    init(navigation: UINavigationController, useCaseProvider: UseCaseProvider) {
        self.navigation = navigation
        self.useCaseProvider = useCaseProvider
    }
    
    func goRooms() {
        navigation.popViewController(animated: true)
    }
    
}
