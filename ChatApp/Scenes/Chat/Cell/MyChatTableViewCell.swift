//
//  MyChatTableViewCell.swift
//  ChatApp
//
//  Created by Dung Do on 27/01/2022.
//

import UIKit

class MyChatTableViewCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel!
    
    var message: Message? {
        didSet {
            contentLabel.text = message?.content
        }
    }
    
}
