//
//  OtherChatTableViewCell.swift
//  ChatApp
//
//  Created by Dung Do on 27/01/2022.
//

import UIKit

class OtherChatTableViewCell: UITableViewCell {
    
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var message: Message? {
        didSet {
            contentLabel.text = message?.content
            nameLabel.text = message?.senderID
        }
    }
    
}
