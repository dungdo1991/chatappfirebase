//
//  Session.swift
//  ChatApp
//
//  Created by Dung Do on 27/01/2022.
//

import Foundation

class Session {
    
    static let shared = Session()
    
    private init() {}
    
    private var user: User?
    var currentUser: User? {
        get {
            if let data = UserDefaults.standard.data(forKey: "currentUser"),
               let user = try? JSONDecoder().decode(User.self, from: data) {
                return user
            }
            return user
        }
        set {
            user = newValue
            let data = try! JSONEncoder().encode(newValue)
            UserDefaults.standard.set(data, forKey: "currentUser")
        }
    }
    
}
