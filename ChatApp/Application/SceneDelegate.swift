//
//  SceneDelegate.swift
//  ChatApp
//
//  Created by Dung Do on 20/01/2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = (scene as? UIWindowScene) else { return }
        
        let roomsVC = RoomsViewController()
        let nav = UINavigationController(rootViewController: roomsVC)
        let provider = FirestoreUseCaseProvider()
        let navigator = DefaultRoomsNavigator(navigation: nav, useCaseProvider: provider)
        roomsVC.roomsVM = RoomsViewModel(userUseCase: provider.createUserUseCase(),
                                         chatUseCase: provider.createChatUseCase(),
                                         navigator: navigator)
        window = UIWindow(windowScene: scene)
        window?.rootViewController = nav
        window?.makeKeyAndVisible()
    }

}

