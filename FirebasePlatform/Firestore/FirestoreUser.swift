//
//  FirebaseUser.swift
//  ChatApp
//
//  Created by Dung Do on 20/01/2022.
//

import Foundation
import FirebaseFirestore
import RxSwift

class FirestoreUser {
    
    private let db = Firestore.firestore()
    
    func createUser() -> Observable<User> {
        Observable.create { observer in
            let refUser: DocumentReference = self.db.collection("users").document()
            let user = User(id: refUser.documentID, date: Date.now)
            try? refUser.setData(from: user) { error in
                if let error = error {
                    observer.onError(error)
                } else {
                    observer.onNext(user)
                }
            }
            return Disposables.create()
        }
    }
    
}
