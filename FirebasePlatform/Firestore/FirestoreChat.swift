//
//  FirebaseChat.swift
//  ChatApp
//
//  Created by Dung Do on 20/01/2022.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift
import RxSwift

class FirestoreChat {
    
    private let db = Firestore.firestore()
    private var listenerRooms: ListenerRegistration?
    private var listenerMessages: ListenerRegistration?
    private var roomCount: Int = 0
    private var chatCount: Int = 0
    
    // MARK: - Room
    
    func createRoom() -> Observable<Room> {
        Observable.create { observer in
            let refRoom: DocumentReference = self.db.collection("rooms").document()
            let room = Room(id: refRoom.documentID, date: Date.now, chat: nil)
            try? refRoom.setData(from: room) { error in
                if let error = error {
                    observer.onError(error)
                } else {
                    observer.onNext(room)
                }
            }
            
            return Disposables.create()
        }
    }
    
    func getRoomFrom(roomID: String) -> Observable<Room?> {
        Observable.create { observer in
            self.db.document("rooms/\(roomID)").getDocument { snapshot, error in
                if let error = error {
                    observer.onError(error)
                } else {
                    let result = Result { try snapshot?.data(as: Room.self) }
                    switch result {
                    case .success(let room):
                        observer.onNext(room)
                    case .failure(let error):
                        observer.onError(error)
                    }
                }
            }
            
            return Disposables.create()
        }
    }
    
    func observerRooms(limit: Int) -> Observable<[Room]> {
        Observable.create { observer in
            let query = self.db.collection("rooms")
                .order(by: "date", descending: true)
                .limit(to: self.roomCount + limit)
            
            self.roomCount += limit
            self.listenerRooms?.remove()
            self.listenerRooms = query.addSnapshotListener { snapshot, error in
                if let error = error {
                    observer.onError(error)
                } else {
                    let rooms = snapshot?.documents.compactMap { document -> Room? in
                        let room = try? document.data(as: Room.self)
                        return room
                    }
                    observer.onNext(rooms ?? [])
                }
            }
            return Disposables.create()
        }
    }
    
    func removeObserverRooms() -> Completable {
        Completable.create { observer in
            self.roomCount = 0
            self.listenerRooms?.remove()
            observer(.completed)
            return Disposables.create()
        }
    }
    
    // MARK: - Chat
    
    func chat(content: String, roomID: String, senderID: String) -> Observable<Message> {
        Observable.create { observer in
            let refGroup: DocumentReference = self.db.document("rooms/\(roomID)")
            let refChat: DocumentReference = refGroup.collection("chat").document()
            let chat = Message(id: refChat.documentID, date: Date.now, senderID: senderID, content: content)
            do {
                refGroup.updateData(["date" : Date.now])
                
                try refChat.setData(from: chat) { error in
                    if let error = error {
                        observer.onError(error)
                    } else {
                        observer.onNext(chat)
                    }
                }
            }
            catch {
                observer.onError(error)
            }
            return Disposables.create()
        }
    }
    
    func observerMessages(from roomID: String, limit: Int) -> Observable<[Message]> {
        Observable.create { observer in
            let query = self.db.collection("rooms/\(roomID)/chat")
                .order(by: "date", descending: true)
                .limit(to: self.roomCount + limit)
            
            self.roomCount += limit
            self.listenerMessages?.remove()
            self.listenerMessages = query.addSnapshotListener { snapshot, error in
                if let error = error {
                    observer.onError(error)
                } else {
                    let messsages = snapshot?.documents.compactMap { document -> Message? in
                        let message = try? document.data(as: Message.self)
                        return message
                    }
                    observer.onNext(messsages ?? [])
                }
            }
            return Disposables.create()
        }
    }
    
    func removeObserverMessages() -> Completable {
        Completable.create { observer in
            self.chatCount = 0
            self.listenerMessages?.remove()
            observer(.completed)
            return Disposables.create()
        }
    }
    
}
