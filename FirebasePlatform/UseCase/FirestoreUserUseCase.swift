//
//  FirestoreUserUseCase.swift
//  ChatApp
//
//  Created by Dung Do on 24/01/2022.
//

import Foundation
import RxSwift

class FirestoreUserUseCase: UserUseCase {
    
    let firestore = FirestoreUser()
    
    func createUser() -> Observable<User> {
        firestore.createUser()
    }
    
}
