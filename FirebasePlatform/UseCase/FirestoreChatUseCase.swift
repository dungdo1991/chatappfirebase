//
//  FirestoreChatUseCase.swift
//  ChatApp
//
//  Created by Dung Do on 24/01/2022.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift
import RxSwift

class FirestoreChatUseCase: ChatUseCase {
    
    let firestore = FirestoreChat()
    
    // MARK: - Rooms
    
    func createRoom() -> Observable<Room> {
        firestore.createRoom()
    }
    
    func observerRooms(skip: Int, limit: Int) -> Observable<[Room]> {
        firestore.observerRooms(limit: limit)
    }
    
    func removeObserverRooms() -> Completable {
        firestore.removeObserverRooms()
    }
    
    func getRoomFrom(roomID: String) -> Observable<Room?> {
        firestore.getRoomFrom(roomID: roomID)
    }
    
    // MARK: - Chat
    
    func chat(content: String, roomID: String, senderID: String) -> Observable<Message> {
        firestore.chat(content: content, roomID: roomID, senderID: senderID)
    }
    
    func observerMessages(from roomID: String, skip: Int, limit: Int) -> Observable<[Message]> {
        firestore.observerMessages(from: roomID, limit: limit)
    }
    
    func removeObserverMessages() -> Completable {
        firestore.removeObserverMessages()
    }
    
}
