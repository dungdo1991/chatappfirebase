//
//  FirestoreUseCaseProvider.swift
//  ChatApp
//
//  Created by Dung Do on 24/01/2022.
//

import Foundation

class FirestoreUseCaseProvider: UseCaseProvider {
    
    func createUserUseCase() -> UserUseCase {
        return FirestoreUserUseCase()
    }
    
    func createChatUseCase() -> ChatUseCase {
        return FirestoreChatUseCase()
    }
    
}
