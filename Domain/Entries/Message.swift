//
//  Message.swift
//  ChatApp
//
//  Created by Dung Do on 20/01/2022.
//

import Foundation
import FirebaseFirestoreSwift

struct Message: Codable {
    let id: String
    let date: Date
    let senderID: String
    let content: String
}
