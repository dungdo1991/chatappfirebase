//
//  UserUseCase.swift
//  ChatApp
//
//  Created by Dung Do on 20/01/2022.
//

import Foundation
import RxSwift

protocol UserUseCase {
    func createUser() -> Observable<User>
}
