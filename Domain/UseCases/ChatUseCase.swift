//
//  ChatUseCase.swift
//  ChatApp
//
//  Created by Dung Do on 20/01/2022.
//

import Foundation
import RxSwift

protocol ChatUseCase {
    // MARK: - Rooms
    func createRoom() -> Observable<Room>
    func observerRooms(skip: Int, limit: Int) -> Observable<[Room]>
    func removeObserverRooms() -> Completable
    func getRoomFrom(roomID: String) -> Observable<Room?>
    
    // MARK: - Chat
    func chat(content: String, roomID: String, senderID: String) -> Observable<Message>
    func observerMessages(from roomID: String, skip: Int, limit: Int) -> Observable<[Message]>
    func removeObserverMessages() -> Completable
}
